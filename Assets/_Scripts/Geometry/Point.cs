﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour {

    private AnglePoolManager anglePoolManager;
    public GameObject linePrefab;
    private Dictionary<GameObject, GameObject> lineDictionary;
    private GameObject angleObject;
    private float timeCreated;
    public float TimeCreated {
        get { return timeCreated; }
        set { this.timeCreated = value; }
    }

    private void OnEnable() {
        timeCreated = Time.time;
        lineDictionary = new Dictionary<GameObject, GameObject>();
        GameObject anglePoolManagerObject = GameObject.Find(StringConstants.ANGLE_POOL);
        anglePoolManager = anglePoolManagerObject.GetComponent<AnglePoolManager>();
    }

    //Add line and angle
    private void OnTriggerEnter(Collider col) {
        if (col.gameObject.tag == StringConstants.TAG_POINT_TRIGGER && col.transform.parent.gameObject != transform.gameObject) {
            Point point = col.transform.parent.gameObject.GetComponent<Point>();
            if (this.timeCreated > point.TimeCreated) {
                GameObject lineObject = Instantiate(linePrefab);
                Line line = lineObject.GetComponentInChildren<Line>();
                line.SetPoints(transform, col.transform);
                lineDictionary.Add(col.gameObject, lineObject);
                point.AddLineFromPoint(this.gameObject, lineObject);
                if (lineDictionary.Count == 2) {
                    CreateAngle(col.transform.parent.gameObject);
                }
            }

        }
    }

    //Remove line and angle
    private void OnTriggerExit(Collider col) {
        if (col.gameObject.tag == StringConstants.TAG_POINT_TRIGGER) {
            GameObject gObject = null;
            if (lineDictionary.TryGetValue(col.transform.parent.gameObject, out gObject)) {
                Line line = gObject.GetComponentInChildren<Line>();
                lineDictionary.Remove(col.transform.parent.gameObject);
                Angle angle = angleObject.GetComponent<Angle>();
                if (angle != null && line != null) {
                    Transform pointTransform;
                    if (line.Point1 == transform) {
                        pointTransform = line.Point2;
                    } else {
                        pointTransform = line.Point1;
                    }

                    if (angle.Point1 == pointTransform || angle.Point2 == pointTransform || angle.Point3 == pointTransform) {
                        angleObject.SetActive(false);
                        angleObject = null;
                    }
                }
                Object.Destroy(gObject);
            }
        }
    }

    //Give the other point a reference to the line made;
    public void AddLineFromPoint(GameObject pointObject, GameObject lineObject) {
        lineDictionary.Add(pointObject, lineObject);
        if (lineDictionary.Count == 2) {
            CreateAngle(pointObject);
        }
    }

    //Create and Angle for connecting lines
    private void CreateAngle(GameObject otherPoint) {
        Transform point1 = null;
        angleObject = anglePoolManager.GetPooledObject();
        Angle angle = angleObject.GetComponent<Angle>();

        foreach (GameObject key in lineDictionary.Keys) {
            GameObject gObject = null;
            if (lineDictionary.TryGetValue(key, out gObject)) {
                Line otherLine = gObject.GetComponentInChildren<Line>();
                if (otherLine != null) {
                    if (otherLine.Point1 == transform) {
                        point1 = otherLine.Point2;
                    } else {
                        point1 = otherLine.Point1;
                    }
                }
                //Only create angle for first 2 lines
                break;
            }
        }
        if (point1 != null) {
            Transform point2 = transform;
            Transform point3 = otherPoint.transform;
            angle.SetPoints(point1, point2, point3);
            angleObject.SetActive(true);
        }
    }
}
