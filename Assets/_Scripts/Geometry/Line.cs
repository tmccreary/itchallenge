﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Line : MonoBehaviour {

    public LineRenderer lineRenderer;
    public Text text;
    public Transform canvasParent;
    private Transform point1;
    private Transform point2;
    public Transform Point1 {
        get { return point1; }
        set { point1 = value; }
    }
    public Transform Point2 {
        get { return point2; }
        set { point2 = value; }
    }

    public void SetPoints(Transform point1, Transform point2) {
        this.point1 = point1;
        this.point2 = point2;
    }
    
    private void Update() {
        lineRenderer.SetPosition(0, point1.position);
        lineRenderer.SetPosition(1, point2.position);
        text.text = Vector3.Distance(point1.position, point2.position).ToString() + "m";
        canvasParent.position = point1.position + ((point2.position - point1.position) / 2) + new Vector3(0f, 0.3f, 0f);
    }
}
