﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Angle : MonoBehaviour {
    public Text text;

    private Transform point1;
    private Transform point2;
    private Transform point3;

    public Transform Point1 {
        get { return point1; }
        set { point1 = value; }
    }
    public Transform Point2 {
        get { return point2; }
        set { point2 = value; }
    }
    public Transform Point3 {
        get { return point3; }
        set { point3 = value; }
    }

    public void SetPoints(Transform point1, Transform point2, Transform point3) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }

    private void Update() {
        Vector3 normal = Vector3.Normalize((point1.position - point2.position) + (point3.position - point2.position));
        transform.position = point2.position - (0.5f * normal);
        float angleDegrees = Vector3.Angle(point1.position - point2.position, point3.position - point2.position);
        text.text = angleDegrees.ToString() + "\u00B0";
    }
}
