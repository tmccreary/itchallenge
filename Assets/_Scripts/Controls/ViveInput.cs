﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveInput : MonoBehaviour {

    protected static ViveInput Instance;

    public static ViveInput getInstance() {
        return Instance;
    }

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        } else {
            gameObject.SetActive(false);
            Debug.Log("You have more than one ViveInput in your scene.");
        }
    }
    
    public delegate void ButtonDownLeftMenu();
    public delegate void ButtonDownRightMenu();
    public delegate void ButtonUpRightMenu();
    public delegate void ButtonDownLeftTrigger();
    public delegate void ButtonDownRightTrigger();
    public delegate void ButtonUpLeftTrigger();
    public delegate void ButtonUpRightTrigger();
    
    public event ButtonDownLeftMenu OnButtonDownLeftMenu;
    public event ButtonDownRightMenu OnButtonDownRightMenu;
    public event ButtonDownRightMenu OnButtonUpRightMenu;
    public event ButtonDownLeftTrigger OnButtonDownLeftTrigger;
    public event ButtonDownRightTrigger OnButtonDownRightTrigger;
    public event ButtonUpLeftTrigger OnButtonUpLeftTrigger;
    public event ButtonUpRightTrigger OnButtonUpRightTrigger;
    
    void Update() {
        CheckControllerEvents();
    }

    protected void CheckControllerEvents() {
        if (Input.GetButtonDown("LeftControllerMenuButton")) {
            if (OnButtonDownLeftMenu != null) {
                OnButtonDownLeftMenu();
            }
        }

        if (Input.GetButtonDown("RightControllerMenuButton")) {
            if (OnButtonDownRightMenu != null) {
                OnButtonDownRightMenu();
            }
        }

        if (Input.GetButtonUp("RightControllerMenuButton")) {
            if (OnButtonUpRightMenu != null) {
                OnButtonUpRightMenu();
            }
        }

        if (Input.GetButtonDown("LeftControllerTrigger")) {
            if (OnButtonDownLeftTrigger != null) {
                OnButtonDownLeftTrigger();
            }
        }

        if (Input.GetButtonDown("RightControllerTrigger")) {
            if (OnButtonDownRightTrigger != null) {
                OnButtonDownRightTrigger();
            }
        }

        if (Input.GetButtonUp("LeftControllerTrigger")) {
            if (OnButtonUpLeftTrigger != null) {
                OnButtonUpLeftTrigger();
            }
        }

        if (Input.GetButtonUp("RightControllerTrigger")) {
            if (OnButtonUpRightTrigger != null) {
                OnButtonUpRightTrigger();
            }
        }
    }
}
