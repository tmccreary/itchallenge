﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {
    public ViveInput viveInput;
    public LineRenderer lineRenderer;
    public LayerMask layerMask;
    public Transform rightHand;
    public Transform rig;
    private bool showTeleport;
    private bool teleport;

    void Start() {
        showTeleport = false;
    }

    void OnEnable() {
        viveInput.OnButtonDownRightMenu += ShowTeleport;
        viveInput.OnButtonUpRightMenu += ExecuteTeleport;
    }

    void OnDisable() {
        viveInput.OnButtonDownRightMenu -= ShowTeleport;
        viveInput.OnButtonUpRightMenu -= ExecuteTeleport;
    }

    private void Update() {
        if (showTeleport || teleport) {
            RaycastHit hit;
            Ray ray = new Ray();
            ray.origin = rightHand.position;
            ray.direction = rightHand.forward;
            lineRenderer.SetPosition(0, rightHand.position);
            if (Physics.Raycast(ray, out hit, 20f, layerMask)) {
                if (hit.collider != null) {
                    lineRenderer.SetPosition(1, hit.point);
                    if (teleport) {
                        rig.position = new Vector3(hit.point.x, rig.position.y, hit.point.z);
                        teleport = false;
                    }
                }
                lineRenderer.enabled = true;
                teleport = false;
            } else {
                lineRenderer.enabled = false;
                teleport = false;
            }
        } else {
            lineRenderer.enabled = false;
            teleport = false;
        }
    }

    private void ShowTeleport() {
        showTeleport = true;
    }

    private void ExecuteTeleport() {
        teleport = true;
        showTeleport = false;
    }
}
