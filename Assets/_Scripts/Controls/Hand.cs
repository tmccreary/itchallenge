﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour {

    public ViveInput viveInput;
    public enum HandEnum { LeftHand, Righthand };
    public HandEnum hand;

    private GameObject pointInHand;
    private bool holdingPoint;

    private void Awake() {
        holdingPoint = false;
    }

    private void OnTriggerStay(Collider col) {
        if (!holdingPoint && col.gameObject.tag == StringConstants.TAG_POINT) {
            pointInHand = col.gameObject;
        }
    }

    private void OnTriggerExit(Collider col) {
        if (col.gameObject == pointInHand) {
            pointInHand = null;
        }
    }

    private void GrabPoint() {
        if (pointInHand != null) {
            pointInHand.transform.parent = transform;
            holdingPoint = true;
        }
    }

    private void ReleasePoint() {
        if (pointInHand != null) {
            pointInHand.transform.parent = null;
            holdingPoint = false;
        }
    }

    void OnEnable() {
        if (hand == HandEnum.LeftHand) {
            viveInput.OnButtonDownLeftTrigger += GrabPoint;
            viveInput.OnButtonUpLeftTrigger += ReleasePoint;
        } else {
            viveInput.OnButtonDownRightTrigger += GrabPoint;
            viveInput.OnButtonUpRightTrigger += ReleasePoint;
        }
    }

    void OnDisable() {
        if (hand == HandEnum.LeftHand) {
            viveInput.OnButtonDownLeftTrigger -= GrabPoint;
            viveInput.OnButtonUpLeftTrigger -= ReleasePoint;
        } else {
            viveInput.OnButtonDownRightTrigger -= GrabPoint;
            viveInput.OnButtonUpRightTrigger -= ReleasePoint;
        }
    }
}
