﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringConstants {
    public const string TAG_POINT = "Point";
    public const string TAG_POINT_TRIGGER = "PointTrigger";
    public const string TAG_HAND = "Hand";
    public const string ANGLE_POOL = "AnglePool";
}