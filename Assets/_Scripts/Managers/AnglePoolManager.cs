﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnglePoolManager : MonoBehaviour {
    public GameObject angleObject;
    private List<GameObject> anglePool;
    public int amountToPool;

    private void Start() {
        anglePool = new List<GameObject>();
    }

    public GameObject GetPooledObject() {
        for (int i = 0; i < anglePool.Count; i++) {
            if (!anglePool[i].activeInHierarchy) {
                return anglePool[i];
            }
        }

        int countBeforeAdding = anglePool.Count;
        CreateAngles();
        return anglePool[countBeforeAdding];
    }

    private void CreateAngles() {
        for (int i = 0; i < amountToPool; i++) {
            GameObject ob = Instantiate(angleObject);
            ob.SetActive(false);
            anglePool.Add(ob);
        }
    }
}
