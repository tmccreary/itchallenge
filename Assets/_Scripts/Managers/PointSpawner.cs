﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointSpawner : MonoBehaviour {

    public ViveInput viveInput;
    public Transform hmd;
    public GameObject pointPrefab;
    public float pointSpawnDistanceFromHead;

    void OnEnable() {
        viveInput.OnButtonDownLeftMenu += SpawnPoint;
    }

    void OnDisable() {
        viveInput.OnButtonDownLeftMenu -= SpawnPoint;
    }

    public void SpawnPoint() {
        Instantiate(pointPrefab, (hmd.forward * pointSpawnDistanceFromHead) + hmd.transform.position, hmd.transform.rotation);
    }
}
