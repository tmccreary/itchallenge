﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {
    private Transform mainCamera;

    void Start() {
        mainCamera = Camera.main.transform;
    }
    
    void LateUpdate() {
        if (mainCamera != null) {
            transform.rotation = Quaternion.LookRotation(transform.position - mainCamera.position);
        }
    }
}
